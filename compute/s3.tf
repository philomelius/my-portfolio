###
# S3 bucket for Cloudfront origin
###

resource "aws_s3_bucket" "cloudfront_bucket" {
    bucket = local.cloudfront_bucket
    acl    = "public-read"

    versioning {
        enabled = true
    }

    server_side_encryption_configuration {

        rule {
        apply_server_side_encryption_by_default {
            sse_algorithm = "AES256"
            }
       }
    }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
    bucket = aws_s3_bucket.cloudfront_bucket.id
    policy = jsonencode({
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "AllowPublicRead",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${local.cloudfront_bucket}/*"
    }
  ]
    })
}