locals {
    apps_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_c.id,
    ] 
}

locals {
    ssh_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_c.id,
    ]
}

locals {
    lb_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.lb_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.lb_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.lb_az_c.id
    ]
}

locals {
  common_tags = {
    "Solution" = "Production"
    "Tier"     = "Compute"
    "POC"      = "Nicolas Hommais nicoommais@gmail.com"
  }

  common_tags_as_list = [
    for k, v in local.common_tags : {
      key                 = k
      value               = v
      propagate_at_launch = true
    }
  ]
}

locals {
  cloudfront_bucket = "cloudfront-bucket-nh"
}
