###
# Autoscaling groups
###

    # Apps

resource "aws_autoscaling_group" "apps" {
    name                        = "apps_ASG"    
    launch_configuration        = aws_launch_configuration.main.id
    vpc_zone_identifier         = local.apps_subnets
    health_check_grace_period   = 60
    target_group_arns           = [aws_lb_target_group.target.id]
    health_check_type           = "ELB"
    
    min_size                    = 1
    max_size                    = 12
    

    lifecycle {
        create_before_destroy   = true
    }

    tags = concat([{
    key                 = "Name"
    value               = "apps"
    propagate_at_launch = true
  }], local.common_tags_as_list)
}

resource "aws_autoscaling_policy" "apps" {
    name                    = "apps-autoscaling-policy"
    policy_type             = "TargetTrackingScaling"
    autoscaling_group_name  = aws_autoscaling_group.apps.id
  
    target_tracking_configuration {
        predefined_metric_specification {
        predefined_metric_type = "ASGAverageCPUUtilization"
        }

        target_value = 75.0
    }
}


    # SSH
resource "aws_autoscaling_group" "bastion" {
    name                    = "bastion"
    launch_configuration    = aws_launch_configuration.ssh.id
    vpc_zone_identifier     = local.ssh_subnets
    target_group_arns       = [aws_lb_target_group.network.id]
    health_check_type       = "ELB"


    min_size            = 1
    max_size            = 6
    desired_capacity    = 3

    lifecycle {
        create_before_destroy = true
    }

    tags = concat([{
    key                 = "Name"
    value               = "bastion"
    propagate_at_launch = true
  }], local.common_tags_as_list)
}
