variable "region" {
    default = "eu-west-3"
}

variable "instance_type" {
    default = "t2.micro"
}

variable "bastion_type" {
    default = "t2.micro"
}

variable "image_id" {
     default = "ami-04552009264cbe9f4" #centos 7 official
}

variable "key_name" {
    default = "wordpress"
}

variable "domain" {
    default = "nicolashommais.com"
}

variable "bucket_name" {
    default = "ops-ng-cloudfront"
}

variable "create_www_record" {
    default = true
}
