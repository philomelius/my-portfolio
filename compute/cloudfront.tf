
resource "aws_cloudfront_distribution" "cloudfront" {
  origin {
      origin_id     = aws_lb.lb.id
      domain_name   = aws_lb.lb.dns_name
      # domain_name   = "${var.bucket_name}.s3.amazonaws.com"

      custom_origin_config {
        http_port                 = 80
        https_port                = 443
        origin_protocol_policy    = "match-viewer"
        origin_ssl_protocols      = ["TLSv1.2"]
        origin_keepalive_timeout  = 5
        origin_read_timeout       = 30

    }
  }

  viewer_certificate {
    acm_certificate_arn             = aws_acm_certificate.cert.arn
    cloudfront_default_certificate  = false
    minimum_protocol_version        = "TLSv1"
    ssl_support_method              = "sni-only"
  }

  restrictions {
      geo_restriction {
        restriction_type = "none"
      }
  }

  aliases = ["www.nicolashommais.com"]

  default_cache_behavior {
    allowed_methods             = ["GET", "HEAD", "OPTIONS"]
    cached_methods              = ["GET", "HEAD"]
    target_origin_id            = aws_lb.lb.id
    min_ttl                     = 0
    default_ttl                 = 86400
    max_ttl                     = 31536000
    compress                    = true
    
    viewer_protocol_policy      = "allow-all"

    forwarded_values {
      headers       = ["*"]
      query_string  = true
      cookies {
        forward     = "all"
      }
   }       
  }
  price_class               = "PriceClass_100"
  enabled                   = true  # accepting end user requests for content, what is that?
  default_root_object       = "index.html"
  is_ipv6_enabled           = false
  retain_on_delete          = false
  

}