###
# route53 records
###

resource "aws_route53_record" "www" {
     count   = var.create_www_record ? 1 : 0
     zone_id = "Z09455071HECHV9GVHT8N"
     name    = "www.${var.domain}"
     type    = "CNAME"
     ttl     = "60"
     records = [aws_cloudfront_distribution.cloudfront.domain_name]    
}
