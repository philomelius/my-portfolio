###
# Launch configurations
###

resource "aws_launch_configuration" "main" {
    instance_type               = var.instance_type
    name                        = "Apps"
    image_id                    = var.image_id
    key_name                    = var.key_name
    security_groups             = [data.terraform_remote_state.security.outputs.security_group_apps]
    user_data                   = file("test_ec2.sh")
    associate_public_ip_address = false

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_launch_configuration" "ssh"{
    instance_type               = var.bastion_type
    name                        = "Bastion"
    image_id                    = var.image_id
    key_name                    = var.key_name
    security_groups             = [data.terraform_remote_state.security.outputs.security_group_bastion]
    # user_data                   = file("start_wordpress.sh")
    associate_public_ip_address = false

    lifecycle {
        create_before_destroy = true
    }
}