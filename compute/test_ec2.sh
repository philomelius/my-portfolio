#!/bin/bash

# Install Apache

sudo yum install httpd -y

sudo systemctl start httpd
sudo systemctl enable httpd

# Download and place index.html

sudo yum install wget -y

sudo wget https://gitlab.com/philomelius/my-public-repo/-/raw/master/misc/index.html
sudo mv index.html /var/www/html/

sudo systemctl httpd restart

