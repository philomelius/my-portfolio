###
# Load balancers
###


###
# Application LB
###

resource "aws_lb" "lb" {
    name    = "lb-app"
    load_balancer_type                  = "application"
    security_groups                     = [data.terraform_remote_state.security.outputs.security_group_lb]
    subnets                             = local.lb_subnets
    
    tags = {
        Name = "lb"
    }
}

    # Listener

resource "aws_lb_listener" "http" {
    load_balancer_arn   = aws_lb.lb.arn 
    port                = "80"
    protocol            = "HTTP"

    default_action {
      type = "redirect"

      redirect {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
    }
  }
}

    # Target group

resource "aws_lb_target_group" "target" {
    vpc_id      = data.terraform_remote_state.networking.outputs.vpc.example.id
    name        = "target-group"
    port        = "80"
    protocol    = "HTTP"
    
    health_check {
        path                = "/"
        protocol            = "HTTP"
        matcher             = "200"
        interval            = 15
        timeout             = 3
        healthy_threshold   = 3
        unhealthy_threshold = 2
    }
}


###
# Network LB
###

resource "aws_lb" "lb_network" {
    name                                = "lb-network"
    load_balancer_type                  = "network"
    internal                            = false
    subnets                             = local.lb_subnets
    enable_cross_zone_load_balancing = true
}

    # Listener

resource "aws_lb_listener" "ssh" {
    load_balancer_arn   = aws_lb.lb_network.arn 
    port                = "22"
    protocol            = "TCP_UDP"

    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.network.arn
    }
  }

    # Target group

resource "aws_lb_target_group" "network" {
    vpc_id      = data.terraform_remote_state.networking.outputs.vpc.example.id
    name        = "target-group-network"
    port        = "22"
    protocol    = "TCP_UDP"

    lifecycle {
        create_before_destroy = true
    }
}

