output "cloudfront_ID" {
    value = aws_cloudfront_distribution.cloudfront.hosted_zone_id
}

output "network_load_balancer" {
    value = aws_lb.lb_network.dns_name
}

output "app_load_balancer" {
    value = aws_lb.lb.dns_name
}

output "success" {
    value = "This is working. You're awesome"
}