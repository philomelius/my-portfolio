locals {
    db_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_c.id
    ]
}

locals {
  common_tags = {
    "Solution" = "Production"
    "Tier"     = "Data"
    "POC"      = "Nicolas Hommais nicoommais@gmail.com"
  }
}


locals {
    success = "This is working. You got this."
}



