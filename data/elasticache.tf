
###
# Elasticache
###

    # Subnet groups
resource "aws_elasticache_subnet_group" "bar" {
  name       = "tf-test-cache-subnet"
  subnet_ids = local.db_subnets
  
}
  
    # Memcached cluster

resource "aws_elasticache_cluster" "example" {
  cluster_id           = "wordpress-memcached-cluster"
  engine               = "memcached"
  node_type            = "cache.t2.micro"
  num_cache_nodes      = 3
  parameter_group_name = "default.memcached1.6"
  port                 = 11211
  az_mode              = "cross-az"
}
