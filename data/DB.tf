###
# Database
###

resource "random_string" "password" {
  length            = 16
  special           = false
  override_special  = "/@'" 
}

    # Main
resource "aws_db_instance" "db" {
    identifier          = "wordpress"
    name                = "wordpress"
    allocated_storage   = 20
    storage_type        = "gp2"
    instance_class      = "db.t2.micro"
    engine              = "mysql"
    engine_version      = "5.7"

    username            = "wordpress"
    password            = random_string.password.result


    multi_az                        = true
    skip_final_snapshot             = true
    enabled_cloudwatch_logs_exports = ["audit", "general"]
    backup_retention_period         = 7
    delete_automated_backups        = true     # set to false in production

    vpc_security_group_ids      = [data.terraform_remote_state.security.outputs.security_group_db]
    db_subnet_group_name        = aws_db_subnet_group.dbgroup.id
    apply_immediately           = true

    tags = {
        Name    = "DB"
    }
}

    # Replica
resource "aws_db_instance" "db_replica" {
    instance_class      = "db.t2.micro"
    replicate_source_db = aws_db_instance.db.id

    multi_az                        = true
    skip_final_snapshot             = true
    enabled_cloudwatch_logs_exports = ["error"]
    backup_retention_period         = 7
    delete_automated_backups        = true     # set to false in production

    vpc_security_group_ids      = [data.terraform_remote_state.security.outputs.security_group_db]
    apply_immediately           = true


    tags = {
        Name    = "DB_replica"
    }
}

    # DB subnet group
resource "aws_db_subnet_group" "dbgroup" {
    name        = "dbgroup"
    subnet_ids  = local.db_subnets
}

