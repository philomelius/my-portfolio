###
# EFS
###

    # File system
    
resource "aws_efs_file_system" "efs_wordpress" {
   creation_token = "efs_wordpress"
   performance_mode = "generalPurpose"
   throughput_mode = "bursting"
   encrypted = "true"
 
 tags = {
     Name = "efs-wordpress"
   }

 }

    # Multi-AZ mount targets

resource "aws_efs_mount_target" "efs_mount_target_az_a" {
  file_system_id    = aws_efs_file_system.efs_wordpress.id
  subnet_id         = data.terraform_remote_state.networking.outputs.subnets.web_db_az_a.id
  security_groups   = [data.terraform_remote_state.security.outputs.security_group_apps]
}

resource "aws_efs_mount_target" "efs_mount_target_az_b" {
  file_system_id    = aws_efs_file_system.efs_wordpress.id
  subnet_id         = data.terraform_remote_state.networking.outputs.subnets.web_db_az_b.id
  security_groups   = [data.terraform_remote_state.security.outputs.security_group_apps]
}

resource "aws_efs_mount_target" "efs_mount_target_az_c" {
  file_system_id    = aws_efs_file_system.efs_wordpress.id
  subnet_id         = data.terraform_remote_state.networking.outputs.subnets.web_db_az_c.id
  security_groups   = [data.terraform_remote_state.security.outputs.security_group_apps]
}

