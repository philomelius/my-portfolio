output "rds_endpoint" {
    value = aws_db_instance.db.endpoint
}

output "replica_endpoint" {
    value = aws_db_instance.db_replica.endpoint
}

output "success" {
    value = "Everything A-OK. Because you're super cool."
}