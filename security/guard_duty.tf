
resource "aws_guardduty_detector" "example" {
  enable = true
  finding_publishing_frequency  = "FIFTEEN_MINUTES"
}
