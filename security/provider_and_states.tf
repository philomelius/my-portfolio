provider "aws" {
    region = var.region
}

terraform {
    backend "s3" {
        bucket         = "aws-templates-ops-state-files"
        region         = "eu-west-3"
        key            = "security/terraform.tfstate"
        dynamodb_table = "dynamo"
        encrypt        = true
  }
}

data "terraform_remote_state" "networking" {
  backend = "s3"

  config = {
    bucket = "aws-templates-ops-state-files"
    region = "eu-west-3"
    key    = "networking/terraform.tfstate"
  }
}
