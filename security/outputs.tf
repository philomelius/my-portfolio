
output "security_group_apps" {
    value = aws_security_group.apps.id
}

output "security_group_bastion" {
    value = aws_security_group.bastion.id
}

output "security_group_lb" {
    value = aws_security_group.lb.id
}

output "security_group_db" {
    value = aws_security_group.db.id
}


output "success" {
    value   = var.success
}