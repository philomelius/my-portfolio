
data "aws_caller_identity" "current" {}

resource "aws_cloudtrail" "example" {
  name                          = "cloud-trail"
  s3_bucket_name                = aws_s3_bucket.cloud_trail.id
  include_global_service_events = false
}

resource "aws_s3_bucket" "cloud_trail" {
  bucket        = local.cloudtrail_bucket
  force_destroy = true
}

resource "aws_s3_bucket_policy" "cloud_trail_bucket_policy" {
  bucket        = aws_s3_bucket.cloud_trail.id

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::${local.cloudtrail_bucket}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${local.cloudtrail_bucket}/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}
