# Security groups

    # apps

resource "aws_security_group" "apps" {
    name    = "SG-apps"
    vpc_id  = data.terraform_remote_state.networking.outputs.vpc.example.id


    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = local.bastion_cidr
    }

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = local.lb_cidr
    }

    
    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = local.lb_cidr
    }
    
    ingress {
        from_port   = 0
        to_port     = 0
        protocol    = "tcp"
        cidr_blocks = local.db_cidr
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"] #rerouted to NAT gateway
    }
}

    # DB

resource "aws_security_group" "db" {
    name    = "SG-db"
    vpc_id  = data.terraform_remote_state.networking.outputs.vpc.example.id

    ingress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = local.apps_cidr
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"] #rerouted to NAT gateway
    }
}

    # bastion

resource "aws_security_group" "bastion" {
    name    = "SG-bastion"
    vpc_id  = data.terraform_remote_state.networking.outputs.vpc.example.id

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 8
        to_port     = 0
        protocol    = "icmp"
        cidr_blocks = concat(local.bastion_cidr, local.apps_cidr, local.db_cidr)
        }

    
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks  = ["0.0.0.0/0"]
    }
}

    # Load balancer dedicated security group

resource "aws_security_group" "lb" {
    name    = "SG-lb"
    vpc_id  = data.terraform_remote_state.networking.outputs.vpc.example.id

    ingress {
        from_port   = 80 
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 443 
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 8
        to_port     = 0
        protocol    = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
        }


    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}