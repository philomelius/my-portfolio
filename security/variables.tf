variable "region" {
    default = "eu-west-3"
}

variable "success" {
    default = "This is working. Because you're awesome."
}