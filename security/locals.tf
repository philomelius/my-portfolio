variable "server_port" {
    default = "80"
}

locals {  
    cloudtrail_bucket = "cloud-trail-bucket-nh"
}

locals {
    bucket_name = "finalcloud-state-files"
}

locals {
    bastion_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_c.id,
    ]
}

locals {
    apps_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_c.id,
    ]
}

locals {
    db_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_c.id,
    ]
}

locals {
    lb_subnets = [
        data.terraform_remote_state.networking.outputs.subnets.lb_az_a.id,
        data.terraform_remote_state.networking.outputs.subnets.lb_az_b.id,
        data.terraform_remote_state.networking.outputs.subnets.lb_az_c.id,
    ]
}

locals {
    bastion_cidr = [
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_a.cidr,
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_b.cidr,
        data.terraform_remote_state.networking.outputs.subnets.bastion_az_c.cidr,
    ]
}

locals {
    apps_cidr = [
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_a.cidr,
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_b.cidr,
        data.terraform_remote_state.networking.outputs.subnets.web_app_az_c.cidr,
    ]
}

locals {
    db_cidr = [
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_a.cidr,
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_b.cidr,
        data.terraform_remote_state.networking.outputs.subnets.web_db_az_c.cidr,
    ]
}

locals {
    lb_cidr = [
        data.terraform_remote_state.networking.outputs.subnets.lb_az_a.cidr,
        data.terraform_remote_state.networking.outputs.subnets.lb_az_b.cidr,
        data.terraform_remote_state.networking.outputs.subnets.lb_az_c.cidr,
    ]
}

locals {
  common_tags = {
    "Solution" = "Production"
    "Tier"     = "Security"
    "POC"      = "Nicolas Hommais nicoommais@gmail.com"
  }
}
