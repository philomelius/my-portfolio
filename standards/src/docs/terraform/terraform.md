Our Terraform code is split into five tiers:

1. Meta
1. Networking
1. Security
1. Data
1. Compute

Each tier plays a role in building out an environment. The tiers have to be built in specific order.

## Meta

This tier focuses on building out two resources:

* An S3 Bucket
* A DynamoDB (DDB) Table

These are used to provide state storage (S3) and locking (DDB) for the other tiers.

This is the only tier that has its `terraform.tfstae` file stored in the Git repository.

## Networking

The networking tier builds out as much as possible of the networking resources: DNS Zones, VPCs, subnets, route tables, etc.

There are some things that are technically network related but aren't managed in the networking tier. This is to make things easier and in some cases, possible. Some things that aren't in the Networking tier:

1. Route53 Records
1. Application and Network Load Balancers

## Security

Anything to do with security is handled in this tier. That includes NACLs and SGs even though they're technically "networking" they're sensitive objects and as such they should be managed carefully.

## Data

Anything to do with data persistence is held in this tier. This includes caching servers/services, RDS, etc.

## Compute

This is where the applications are housed. This is also where the load balancers are managed because LBs are tightly coupled with the application they may as well be as dynamically/freely allocated as compute resources.

## Example

See [the following repository](https://gitlab.com/opsfactory-as-code/fictional-pty-ltd/production-infrastructure) for an example of this structure in action/