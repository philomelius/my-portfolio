There are (currently) two situations that require a set of architecture diagrams:

1. When creating a new template
1. When onboarding a new customer

These diagrams need to be designed in a specific manner.

## New Template

When we create a new template, we create an environment within the Fictional Pty Ltd "organisation". This allows us to produce the final product and test it's working as we expect.

The templates we create are complete solutions which can be bought off the shelve as a one time cost. As a result they require documentation and diagrams.

## New Customer

Customers have their solutions based off of templates. They can customise these templates of course, but some base level architectural diagram is still required, but optional. Customer can opt for a code only solution.

For customers who opt to have a custom diagram designed this document outlines the standards to which they're developed.

## Standards

These are the standards at a high level. We need diagrams dmeonstrating:

1. A bird's eye view with **no traffic lines**
1. External Internet access **into** the environment
1. External Internet access **out of** the environment
1. Access between load balancers and backend servers (using traffic lines)
1. Access between SSH/RDP bastions and internal systems
1. Logging activity and their path into S3 or storage backend
1. Access between application servers and data tiers
1. Access between all EC2 instances and external update servers

Below are examples from the Fictional Pty Ltd solution.

!!! note
    These examples might not be consistent or accurate but that's OK, they're mainly style guides.

### Bird's Eye View

This diagram should not contain traffic flow lines. The idea is to give a visualisation of what's going on at a high level. All the technologies can be clearly seen without lines getting in the way.

From here we then break down the architecture into sub components as seen below. These components then contain the traffic lines and detailed information, but not **too* detailed.

![birds-eye-view](images/production_hld.png)

### Inbound External Internet Access
![inbound-external-internet](images/production_internet_traffic_flow.png)

### Outbound External Internet Access
![outbound-external-internet](images/production_outbound_internet_traffic_flow.png)

### Loadbalancers and Applications
![inbound-external-internet](images/production_app_tier_traffic_flow.png)

### Bastions and Internal Systems
![bastion-traffic-flow](images/production_bastion_traffic_flow.png)

### Logging Activity and Flows
![logging-traffic-flow](images/production_logging_traffic_flow.png)

### Application Servers and Data Tier
![data-traffic-flow](images/production_data_tier_traffic_flow.png)

### EC2 and External Services
![ec2-external-service-traffic-flow](images/production_ec2_external_app_traffic_flow.png)
