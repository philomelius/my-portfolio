This documentation describes all the standards that everything is developed to. This includes:

* Architectural digrams
* Terraform code
* Python code
* MkDocs documentation (the stuff that isn't auto-generated)

And more.

## Fictional Pty Ltd

When we create new templates we place working examples of them under a fictional company called Fictional Pty Ltd. We have the TLD `fictional.com.au`. It doesn't route to anything at all. It's simply available for testing code (for things like ACM certs.)

We use this fake company to showcase, demonstrate and generally house implementations of templates.
