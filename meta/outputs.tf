# S3 outputs

output "region" {
    value   = var.region
}

output "success" {
    value   = "Everything is working fine. Because you're awesome."
}
