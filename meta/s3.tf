# S3 bucket for state files

resource "aws_s3_bucket" "state" {
    bucket  = "aws-templates-ops-state-files"
    acl     = "private"
  # force_destroy = true

    versioning {
        enabled = true
    }

    server_side_encryption_configuration {

        rule {
        apply_server_side_encryption_by_default {
            sse_algorithm = "AES256"
            }
       }
    }
}

