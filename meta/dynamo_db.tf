# DynamoDB for state file management

resource "aws_dynamodb_table" "dynamo" {
    name            = "dynamo"
    billing_mode    = "PAY_PER_REQUEST"
    hash_key        = "LockID"

    attribute {
        name = "LockID"
        type = "S"
    }
}