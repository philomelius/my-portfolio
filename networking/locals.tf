locals {
  subnets = {
    bastion = {
      az_a = "10.0.1.0/24"
      az_b = "10.0.2.0/24"
      az_c = "10.0.3.0/24"
    }
    
    web_app = {
      az_a = "10.0.4.0/24"
      az_b = "10.0.5.0/24"
      az_c = "10.0.6.0/24"
    }

    web_db = {
      az_a = "10.0.7.0/24"
      az_b = "10.0.8.0/24"
      az_c = "10.0.9.0/24"
    }

    lb = {
      az_a = "10.0.10.0/24"
      az_b = "10.0.11.0/24"
      az_c = "10.0.12.0/24"
    }
  }
}

locals {
  availability_zone = "eu-west-3"
}

locals {
  common_tags = {
    "Solution" = "Production"
    "Tier"     = "Networking"
    "POC"      = "Nicolas Hommais nicoommais@gmail.com"
  }
}