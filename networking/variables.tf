variable "region" {
    default = "eu-west-3"
}

variable "domain" {
    default = "nicolashommais.com"
}

variable "success" {
    default = "This is working. Because you're awesome."
}

