output "vpc" {
  description = "VPC info"
  value = {
    example = {
      id    = aws_vpc.example.id
      cidr  = aws_vpc.example.cidr_block
    }
  }
}

output "subnets" {
  description = "All subnets created"
  value = {
    web_app_az_a = {
      id   = aws_subnet.web_app_az_a.id
      cidr = aws_subnet.web_app_az_a.cidr_block
    }

    web_app_az_b = {
      id   = aws_subnet.web_app_az_b.id
      cidr = aws_subnet.web_app_az_b.cidr_block
    }

    web_app_az_c = {
      id   = aws_subnet.web_app_az_c.id
      cidr = aws_subnet.web_app_az_c.cidr_block
    }

    lb_az_a = {
      id   = aws_subnet.lb_az_a.id
      cidr = aws_subnet.lb_az_a.cidr_block
    }

    lb_az_b = {
      id   = aws_subnet.lb_az_b.id
      cidr = aws_subnet.lb_az_b.cidr_block
    }

    lb_az_c = {
      id   = aws_subnet.lb_az_c.id
      cidr = aws_subnet.lb_az_c.cidr_block
    }

    web_db_az_a = {
      id   = aws_subnet.web_db_az_a.id
      cidr = aws_subnet.web_db_az_a.cidr_block
    }

    web_db_az_b = {
      id   = aws_subnet.web_db_az_b.id
      cidr = aws_subnet.web_db_az_b.cidr_block
    }

    web_db_az_c = {
      id   = aws_subnet.web_db_az_c.id
      cidr = aws_subnet.web_db_az_c.cidr_block
    }

    bastion_az_a = {
      id   = aws_subnet.bastion_az_a.id
      cidr = aws_subnet.bastion_az_a.cidr_block
    }

    bastion_az_b = {
      id   = aws_subnet.bastion_az_b.id
      cidr = aws_subnet.bastion_az_b.cidr_block
    }

    bastion_az_c = {
      id   = aws_subnet.bastion_az_c.id
      cidr = aws_subnet.bastion_az_c.cidr_block
    }
  }
}

output "success" {
    value   = var.success
}
