# Route table associations

###
# Internal
###

    # Apps

resource "aws_route_table_association" "web_app_az_a" {
    subnet_id       = aws_subnet.web_app_az_a.id
    route_table_id  = aws_route_table.internal_a.id
}


resource "aws_route_table_association" "web_app_az_b" {
    subnet_id       = aws_subnet.web_app_az_b.id
    route_table_id  = aws_route_table.internal_b.id
}


resource "aws_route_table_association" "web_app_az_c" {
    subnet_id       = aws_subnet.web_app_az_c.id
    route_table_id  = aws_route_table.internal_c.id
}

    # DB
 
resource "aws_route_table_association" "web_db_az_a" {
    subnet_id       = aws_subnet.web_db_az_a.id
    route_table_id  = aws_route_table.internal_a.id
}


resource "aws_route_table_association" "web_db_az_b" {
    subnet_id       = aws_subnet.web_db_az_b.id
    route_table_id  = aws_route_table.internal_b.id
}


resource "aws_route_table_association" "web_db_az_c" {
    subnet_id       = aws_subnet.web_db_az_c.id
    route_table_id  = aws_route_table.internal_c.id
}


###
# Internet facing
###

    # Bastion

resource "aws_route_table_association" "bastion_az_a" {
    subnet_id       = aws_subnet.bastion_az_a.id
    route_table_id  = aws_route_table.internet_facing.id
}

resource "aws_route_table_association" "bastion_az_b" {
    subnet_id       = aws_subnet.bastion_az_b.id
    route_table_id  = aws_route_table.internet_facing.id
}

resource "aws_route_table_association" "bastion_az_c" {
    subnet_id       = aws_subnet.bastion_az_c.id
    route_table_id  = aws_route_table.internet_facing.id
}

    # LB

resource "aws_route_table_association" "lb_az_a" {
    subnet_id       = aws_subnet.lb_az_a.id
    route_table_id  = aws_route_table.internet_facing.id
}

resource "aws_route_table_association" "lb_az_b" {
    subnet_id       = aws_subnet.lb_az_b.id
    route_table_id  = aws_route_table.internet_facing.id
}

resource "aws_route_table_association" "lb_az_c" {
    subnet_id       = aws_subnet.lb_az_c.id
    route_table_id  = aws_route_table.internet_facing.id
}