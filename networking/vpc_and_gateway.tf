resource "aws_vpc" "example" {
  cidr_block  = "10.0.0.0/16"

  tags = {
        Name = "Example"
    }
}

resource "aws_internet_gateway" "igw"{
  vpc_id    = aws_vpc.example.id 

  tags = {
      Name = "Example_IGW"
  }
}

