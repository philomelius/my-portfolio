###
# NAT gateways
###

resource "aws_nat_gateway" "nat_a" {
    subnet_id               = aws_subnet.bastion_az_a.id
    allocation_id           = aws_eip.nat_eip_a.id
        
    tags= {
        Name = "nat_gateway_a"
    }
}

resource "aws_nat_gateway" "nat_b" {
    subnet_id               = aws_subnet.bastion_az_b.id
    allocation_id           = aws_eip.nat_eip_b.id
        
    tags= {
        Name = "nat_gateway_b"
    }
}

resource "aws_nat_gateway" "nat_c" {
    subnet_id               = aws_subnet.bastion_az_c.id
    allocation_id           = aws_eip.nat_eip_c.id
        
    tags= {
        Name = "nat_gateway_c"
    }
}

###
# Network interfaces
###

resource "aws_network_interface" "net_interface_a" {
    subnet_id       = aws_subnet.bastion_az_a.id
    # security_groups = [aws_security_group.dmz.id]
    
    tags = {
        Name = "network_interface_a"
    }
}

resource "aws_network_interface" "net_interface_b" {
    subnet_id       = aws_subnet.bastion_az_b.id
    # security_groups = [aws_security_group.dmz.id]
    
    tags = {
        Name = "network_interface_b"
    }
}

resource "aws_network_interface" "net_interface_c" {
    subnet_id       = aws_subnet.bastion_az_c.id
    # security_groups = [aws_security_group.dmz.id]
    
    tags = {
        Name = "network_interface_c"
    }
}


###
# Elastic IPs
###

resource "aws_eip" "nat_eip_a" {
    depends_on  = [aws_internet_gateway.igw]
    vpc         = true

    tags = {
        Name = "nat_eip_a"
    }
}

resource "aws_eip" "nat_eip_b" {
    depends_on  = [aws_internet_gateway.igw]
    vpc         = true

    tags = {
        Name = "nat_eip_b"
    }

}

resource "aws_eip" "nat_eip_c" {
    depends_on  = [aws_internet_gateway.igw]
    vpc         = true

    tags = {
        Name = "nat_eip_c"
    }
}
