# SUBNETS

###
# Apps
###

resource "aws_subnet" "web_app_az_a" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.web_app.az_a
    availability_zone   = "${local.availability_zone}a"
    
    tags = {
    Name = "web_app_az_a"
  } 
}

resource "aws_subnet" "web_app_az_b" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.web_app.az_b
    availability_zone   = "${local.availability_zone}b"

    tags = {
    Name = "web_app_az_b"
  }
}

resource "aws_subnet" "web_app_az_c" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.web_app.az_c
    availability_zone   = "${local.availability_zone}c"

    tags = {
    Name = "web_app_az_c"
  }
}

###
# Data
###

resource "aws_subnet" "web_db_az_a" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.web_db.az_a
    availability_zone   = "${local.availability_zone}a"

    tags = {
    Name = "web_db_az_a"
  }

}

resource "aws_subnet" "web_db_az_b" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.web_db.az_b
    availability_zone   = "${local.availability_zone}b"

    tags = {
    Name = "web_db_az_b"
  }

}

resource "aws_subnet" "web_db_az_c" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.web_db.az_c
    availability_zone   = "${local.availability_zone}c"

    tags = {
    Name = "web_db_az_c"
  }

}

###
# Bastion
###

resource "aws_subnet" "bastion_az_a" {
    vpc_id              = aws_vpc.example.id 
    cidr_block          = local.subnets.bastion.az_a
    availability_zone   = "${local.availability_zone}a"

    tags = {
    Name = "bastion_az_a"
  }
}

resource "aws_subnet" "bastion_az_b" {
    vpc_id              = aws_vpc.example.id 
    cidr_block          = local.subnets.bastion.az_b
    availability_zone   = "${local.availability_zone}b"

    tags = {
    Name = "bastion_az_b"
  }
}

resource "aws_subnet" "bastion_az_c" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.bastion.az_c
    availability_zone   = "${local.availability_zone}c"

    tags = {
    Name = "bastion_az_c"
  }

}

###
# Load balancers
###

resource "aws_subnet" "lb_az_a" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.lb.az_a
    availability_zone   = "${local.availability_zone}a"

    tags = {
    Name = "lb_az_a"
  }
}

resource "aws_subnet" "lb_az_b" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.lb.az_b
    availability_zone   = "${local.availability_zone}b"

    tags = {
    Name = "lb_az_b"
  }
}

resource "aws_subnet" "lb_az_c" {
    vpc_id              = aws_vpc.example.id
    cidr_block          = local.subnets.lb.az_c
    availability_zone   = "${local.availability_zone}c"

    tags = {
    Name = "lb_az_c"
  }
}