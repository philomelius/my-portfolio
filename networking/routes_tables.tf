# Routing tables


###
# Internet facing
###


resource "aws_route_table" "internet_facing" {
    vpc_id  = aws_vpc.example.id 

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.igw.id
    }

    tags = {
        Name = "public_route_table"
    }
} 


###
# Internal (one per AZ)
###

resource "aws_route_table" "internal_a" {
    vpc_id      = aws_vpc.example.id
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id  = aws_nat_gateway.nat_a.id
    }

    tags = {
        Name = "private_route_table_a"
    }
}

resource "aws_route_table" "internal_b" {
    vpc_id      = aws_vpc.example.id
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id  = aws_nat_gateway.nat_b.id
    }

    tags = {
        Name = "private_route_table_b"
    }
}

resource "aws_route_table" "internal_c" {
    vpc_id      = aws_vpc.example.id
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id  = aws_nat_gateway.nat_c.id
    }

    tags = {
        Name = "private_route_table_c"
    }
}